import Vue from "vue";
import App from "./App.vue";

import "./sass/bulma-styles.scss";
import "./css/fontawesome-styles.css";

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
