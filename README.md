# Sequelize Vue Beta Lesson

## Description
Taking Sequelize for a test run with VueJS and Express in the front and back ends respectively.

### Processing and/or completion date(s)
June 27, 2020 to June 28, 2020

## Attribution:
The original exercise was done by the folks at [StackAbuse](https://stackabuse.com/using-sequelize-js-and-sqlite-in-an-express-js-app/ "link to stackabuse"). 

The other project is referred to as Alpha since everything is on the backend and it has no routing in terms of VueJS. It concentrates on the backend and database functionalities for the most part.

This project expands on those lessons learned while formulating the Alpha version. The **vue create** command was utilized to create the front end. Therefore, this project and is not as &quot;organic&quot; as the alpha version and/or the original lesson for that matter.

In essence, I re&#45;expressed the project in order to further find my bearings in terms of VueJS and Sequelize.

## This is one of many baby steps to cure personal shortfalls in deciphering Sequelize errors
I have in recent weeks attempted to complete the 
[ApolloGraphQL fullstack tutorial](https://www.apollographql.com/docs/tutorial/introduction/ "link to tutorial"). In order to understand an error I encountered in **that** lesson, I need to understand Sequelize and how it relates to the bigger picture. **sequelize-cli** was an important part, too, and laid the groundwork for future experimentation with more organic projects developed without the aid of **sequelize-cli**.

Help me if you are able. Otherwise, what some of you mean for evil against me God means it for good and render your evil deeds impotent. God has a plan and an agenda for me. I hope you can say the same.

## God bless!